/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.*;

/**
 * Interface do servidor, onde chama todos os métodos necessários para gerência
 * Consultas de vagas, consulta de curriculum
 * Registro de vagas, registro de curriculum
 * Registro de interesse de vaga e curriculum
 * @author Giovanni e Steven
 */
public interface InterfaceServ extends Remote{

    //Chamadas do Cliente2, que oferece vagas de emprego
    public void ConsultaCurriculo(String areaDeInteresse, InterfaceCli2 cliente) throws RemoteException;
    public void CadastroVaga(String nome, String contato, String areaDeVaga, Integer cargaHoraria, Integer salario, InterfaceCli2 clienteRef) throws RemoteException;
    

    //Chamada de Registro de interesse de curriculum pelo Cliente2
    public void RegistroInteresseCurriculo(String areaDeInteresse,InterfaceCli2 cliente) throws RemoteException;
}
