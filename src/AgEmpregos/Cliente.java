/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * Classe do Cliente2, que faz oferta de vagas de emprego
 * Menu próprio para o Cliente2
 * @author Giovanni e Steven
 */
public class Cliente {//CLIENTE 2 - Cliente que possui uma vaga e quer encontrar curriculos

    /**
     * Função main para execução do lado do cliente2
     * @param args the command line arguments
     * @throws java.rmi.RemoteException
     * @throws java.rmi.NotBoundException
     */
    public static void main(String[] args) throws RemoteException, NotBoundException {
        // TODO code application logic here
        Registry registro = LocateRegistry.getRegistry();
        CliImpl cliente = new CliImpl((InterfaceServ)registro.lookup("Servidor"));
        
        String resposta;//variável que armazena a resposta dada no menu
        
        String nome;                //variavel para armazenar o nome da empresa durante o processo de cadastro
        String contato;             //variavel para armazenar o contato da empresa durante o processo de cadastro
        String areaDeInteresse;     /*variavel para armazenar a area da vaga no processo de cadastro e a area de interesse
                                    em curriculos que serão consultados e a area de interesse a ser salvo no registro de interesse*/
        Integer cargaHoraria;       //variavel para armazenar a carga horaria da vaga no processo de cadastro
        Integer salario;            //variavel para armazenar o salario da vaga durante o processo de cadastro
        
        //menu armazena o texto a ser exibido no menu principal
        String menu = "O que gostaria de fazer?\n 1 - Cadastro\n 2 - Consultar Curriculos\n 3 - Registrar Area de Interesse";
        //scanner para entrada de dados
        Scanner scanner = new Scanner(System.in);
        
        //Inicio de menu
        while(true){//roda infinitamente a aplicação até o processo todo ser cancelado
            System.out.println(menu);//apresenta o menu para o usuario e recebe a resposta que levará a um case específico para resolver a opção escolhida
            resposta = scanner.next();
            switch(resposta){
                case "1"://Cadastro
                    System.out.println("Por favor, preencha os campos do cadastro:");//preenchimento dos campos do cadastro
                    System.out.print("Nome da Empresa: ");
                    nome = scanner.next();
                    System.out.print("Contato: ");
                    contato = scanner.next();
                    System.out.print("Area da Vaga: ");
                    areaDeInteresse = scanner.next();
                    System.out.print("Carga Horaria(Apenas Numero): ");
                    cargaHoraria = scanner.nextInt();
                    System.out.print("Salario(Apenas Numero e Sem Vírgula): ");
                    salario = scanner.nextInt();
                    cliente.servidor.CadastroVaga(nome, contato, areaDeInteresse, cargaHoraria, salario, cliente);
                    
                    //altera o menu principal de "Cadastro" para "Alterar Cadastro"
                    menu = "O que gostaria de fazer?\n 1 - Alterar Cadastro\n 2 - Consultar Curriculos\n 3 - Registrar Area de Interesse";
                    break;
                case "2"://Consultar Curriculos
                    
                    //recebe a area de interesse para pesquisar entre os curriculos
                    System.out.println("Por qual area de interesse deseja procurar?");
                    areaDeInteresse = scanner.next();
                    cliente.servidor.ConsultaCurriculo(areaDeInteresse, cliente);
                            
                    break;
                case "3"://Registrar Area de Interesse
                    
                    //recebe a area de interesse para receber notificacoes sobre curriculos com essa area
                    System.out.println("Qual area você tem interesse em receber notificações?");
                    areaDeInteresse = scanner.next();
                    cliente.servidor.RegistroInteresseCurriculo(areaDeInteresse, cliente);
                    break;
                default:
                    //caso um número não válido seja escolhido o usuário é avisado por ese print
                    System.out.println("Escolha um número válido!");
            }
        }
    }
    
}
